package ap.mobile.made.dictionary;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PreloadActivity extends AppCompatActivity implements LoadTask.IDataLoadListener {

    private boolean firstRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_load);
        this.firstRun = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(getString(R.string.key_pref_first_run), true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadTask(this, this).execute();
    }

    private boolean first = true;
    private int progress = 0;

    @Override
    public void onProgress(int progress) {

        if(this.progress > progress) first = false;
        else this.progress = progress;

        if(this.firstRun) {
            if (first) ((TextView) this.findViewById(R.id.tv_preloading)).setText(R.string.preload_en_id);
            else ((TextView) this.findViewById(R.id.tv_preloading)).setText(R.string.preload_id_en);
            ((ProgressBar) this.findViewById(R.id.pb_preloading_progress)).setProgress(progress);
            ((TextView) this.findViewById(R.id.tv_preloading_progress)).setText(
                    String.format(getResources().getString(R.string.progress_percent), progress));
        } else {
            ((ProgressBar) this.findViewById(R.id.pb_preloading_progress)).setProgress(progress);
            ((TextView) this.findViewById(R.id.tv_preloading)).setText(R.string.starting_up);
            ((TextView) this.findViewById(R.id.tv_preloading_progress)).setText(
                    String.format(getResources().getString(R.string.progress_percent), progress));
        }
    }

    @Override
    public void onLoadComplete() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
        this.finish();
    }
}
