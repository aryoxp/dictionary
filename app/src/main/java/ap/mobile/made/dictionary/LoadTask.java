package ap.mobile.made.dictionary;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;

import ap.mobile.made.dictionary.db.DataHelper;
import ap.mobile.made.dictionary.db.DictionaryHelper;

public class LoadTask extends AsyncTask<Void, Integer, Void> {

    private Context context;
    private IDataLoadListener dataLoadListener;
    private DictionaryHelper dictionaryHelper;

    LoadTask(Context context, IDataLoadListener dataLoadListener) {
        this.context = context;
        this.dataLoadListener = dataLoadListener;
    }

    @Override
    protected void onPreExecute() {
        this.dictionaryHelper = new DictionaryHelper(this.context);
    }

    @Override
    protected Void doInBackground(Void... voids) {

        Boolean firstRun = PreferenceManager.getDefaultSharedPreferences(this.context).getBoolean("pref_first_run", true);

        if (firstRun) {

            this.dictionaryHelper.open();
            this.dictionaryHelper.beginTransaction();
            try {
                this.loadData(false); // preload en to id
                this.loadData(true);  // preload id to en
                this.dictionaryHelper.setTransactionSuccess();
            } catch (Exception ex) {
                Log.e(this.context.getResources().getString(R.string.app_tag), ex.getMessage());
            }
            this.dictionaryHelper.endTransaction();
            this.dictionaryHelper.close();

            // loading will be disabled on next run
            PreferenceManager.getDefaultSharedPreferences(this.context)
                    .edit()
                    .putBoolean("pref_first_run", false)
                    .apply();

        } else {
            try {
                synchronized (this) {
                    publishProgress(100);
                    this.wait(1000);
                }
            } catch (Exception ignored) {}
        }
        return null;
    }

    private void loadData(Boolean reverse) {
        ArrayList<WordEntry> wordEntries = DataHelper.preLoadRaw(this.context, reverse);
        int count = 0, progress = 0;
        float pf;
        for (WordEntry word : wordEntries) {
            this.dictionaryHelper.insert(word, reverse);
            pf = (float) ++count / (float) wordEntries.size();
            if(progress < (int) (pf * 100)) {
                progress = (int) (pf * 100);
                publishProgress(progress);
            }
        }
    }

    @Override
    protected void onProgressUpdate (Integer...values){
        if (this.dataLoadListener != null)
            this.dataLoadListener.onProgress(values[0]);
    }

    @Override
    protected void onPostExecute (Void aVoid){
        if (this.dataLoadListener != null)
            this.dataLoadListener.onLoadComplete();
    }

    public interface IDataLoadListener {
        void onProgress(int progress);
        void onLoadComplete();
    }
}



