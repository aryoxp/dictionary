package ap.mobile.made.dictionary;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class SuggestionAdapter extends RecyclerView.Adapter<SuggestionAdapter.ViewHolder> {

    private ArrayList<WordEntry> wordEntries;
    private ISuggestionListener suggestionListener;

    SuggestionAdapter(ISuggestionListener listener) {
        this.suggestionListener = listener;
        this.wordEntries = new ArrayList<>();
    }

    public void setWords(ArrayList<WordEntry> words) {
        this.wordEntries = words;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvWord;

        ViewHolder(View itemView) {
            super(itemView);
            this.tvWord = itemView.findViewById(R.id.tv_word);
        }

        public void bind(final WordEntry wordEntry, final ISuggestionListener listener) {
            this.tvWord.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(listener != null)
                        listener.onWordClicked(wordEntry);
                }
            });
        }
    }

    public void clearWords() {
        this.wordEntries.clear();
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SuggestionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_word, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SuggestionAdapter.ViewHolder holder, int position) {
        WordEntry wordEntry = this.wordEntries.get(position);
        holder.tvWord.setText(wordEntry.getWord());
        holder.bind(wordEntry, this.suggestionListener);
    }

    @Override
    public int getItemCount() {
        return this.wordEntries.size();
    }

    public interface ISuggestionListener {
        void onWordClicked(WordEntry wordEntry);
    }
}
