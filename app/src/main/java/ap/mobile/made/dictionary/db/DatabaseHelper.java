package ap.mobile.made.dictionary.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static ap.mobile.made.dictionary.db.DatabaseContract.DictionaryColumns.WORD;
import static ap.mobile.made.dictionary.db.DatabaseContract.DictionaryColumns.TRANSLATION;
import static ap.mobile.made.dictionary.db.DatabaseContract.DictionaryColumns._ID;
import static ap.mobile.made.dictionary.db.DatabaseContract.TABLE_NAME;
import static ap.mobile.made.dictionary.db.DatabaseContract.TABLE_NAME_REV;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "dbdictionary";

    private static final int DATABASE_VERSION = 1;

    private static String CREATE_TABLE_DICTIONARY = "create table " + TABLE_NAME +
            " (" + _ID + " integer primary key autoincrement, " +
            WORD + " text not null, " +
            TRANSLATION + " text not null);";

    private static String CREATE_TABLE_DICTIONARY_REV = "create table " + TABLE_NAME_REV +
            " (" + _ID + " integer primary key autoincrement, " +
            WORD + " text not null, " +
            TRANSLATION + " text not null);";

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_DICTIONARY);
        db.execSQL(CREATE_TABLE_DICTIONARY_REV);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_REV);
        onCreate(db);
    }

}
