package ap.mobile.made.dictionary.db;

import android.content.Context;
import android.content.res.Resources;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ap.mobile.made.dictionary.R;
import ap.mobile.made.dictionary.WordEntry;

public class DataHelper {

    public static ArrayList<WordEntry> preLoadRaw(Context context, Boolean reverse) {
        ArrayList<WordEntry> wordEntries = new ArrayList<>();
        try {
            Resources res = context.getResources();
            InputStream rawResource = res.openRawResource(reverse ? R.raw.english_indonesia : R.raw.indonesia_english);
            BufferedReader reader = new BufferedReader(new InputStreamReader(rawResource));
            String line;
            while((line = reader.readLine()) != null) {
                String[] splitstr = line.split("\t");
                WordEntry wordEntry = new WordEntry(splitstr[0], splitstr[1]);
                wordEntries.add(wordEntry);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wordEntries;
    }

}
