package ap.mobile.made.dictionary;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import ap.mobile.made.dictionary.db.DictionaryHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SuggestionAdapter.ISuggestionListener, TextWatcher {

    private SuggestionAdapter suggestionAdapter;
    private boolean translationDirection; // true: EN to ID translation direction
    private DictionaryHelper dictionaryHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        this.translationDirection =
                PreferenceManager.getDefaultSharedPreferences(this)
                        .getBoolean(getString(R.string.key_pref_direction), true);

        setContentView(R.layout.activity_main);

        this.showTranslationDirection();

        this.findViewById(R.id.bt_clear).setOnClickListener(this);
        this.findViewById(R.id.bt_switch_translation).setOnClickListener(this);
        RecyclerView rvSuggestions = this.findViewById(R.id.rv_suggestions);
        rvSuggestions.setLayoutManager(new LinearLayoutManager(this));
        this.suggestionAdapter = new SuggestionAdapter(this);
        rvSuggestions.setAdapter(this.suggestionAdapter);
        this.dictionaryHelper = new DictionaryHelper(this);
        ((EditText)this.findViewById(R.id.et_keyword)).addTextChangedListener(this);

    }

    private void showTranslationDirection() {
        if(this.translationDirection)
            ((TextView)this.findViewById(R.id.bt_switch_translation)).setText(R.string.en_to_id);
        else ((TextView)this.findViewById(R.id.bt_switch_translation)).setText(R.string.id_to_en);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.bt_clear:
                ((EditText)this.findViewById(R.id.et_keyword)).setText("");
                break;
            case R.id.bt_switch_translation:
                this.translationDirection = !this.translationDirection;
                this.showTranslationDirection();
                this.search(((EditText)this.findViewById(R.id.et_keyword)).getText().toString());

                // remember translation direction
                PreferenceManager.getDefaultSharedPreferences(this)
                        .edit()
                        .putBoolean(getString(R.string.key_pref_direction), this.translationDirection)
                        .apply();

                break;
        }
    }

    @Override
    public void onWordClicked(WordEntry wordEntry) {
        ((TextView)this.findViewById(R.id.tv_word)).setText(wordEntry.getWord());
        ((TextView)this.findViewById(R.id.tv_translated)).setText(wordEntry.getTranslation());
        ((EditText)this.findViewById(R.id.et_keyword)).setText(wordEntry.getWord());
        this.suggestionAdapter.clearWords();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {}

    @Override
    public void afterTextChanged(Editable s) {
        this.search(s.toString());
    }

    private void search(String word) {
        this.dictionaryHelper.open();
        ArrayList<WordEntry> entries = this.dictionaryHelper.getTranslation(word, this.translationDirection);
        if(entries.size() > 0)
            this.findViewById(R.id.rv_suggestions).setVisibility(View.VISIBLE);
        this.dictionaryHelper.close();
        this.suggestionAdapter.setWords(entries);
        this.suggestionAdapter.notifyDataSetChanged();
    }

}
