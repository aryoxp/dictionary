package ap.mobile.made.dictionary.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import ap.mobile.made.dictionary.WordEntry;

import static ap.mobile.made.dictionary.db.DatabaseContract.DictionaryColumns.WORD;
import static ap.mobile.made.dictionary.db.DatabaseContract.DictionaryColumns.TRANSLATION;
import static ap.mobile.made.dictionary.db.DatabaseContract.DictionaryColumns._ID;
import static ap.mobile.made.dictionary.db.DatabaseContract.TABLE_NAME;
import static ap.mobile.made.dictionary.db.DatabaseContract.TABLE_NAME_REV;

public class DictionaryHelper {

    private Context context;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    public DictionaryHelper(Context context) {
        this.context = context;
    }

    public DictionaryHelper open() throws SQLException {
        this.databaseHelper = new DatabaseHelper(this.context);
        this.database = this.databaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.databaseHelper.close();
    }

    public ArrayList<WordEntry> getTranslation(String word, Boolean reverse) {
        Cursor cursor = database.query(reverse ? TABLE_NAME_REV : TABLE_NAME, null, WORD + " LIKE ?",
                new String[]{word + '%'}, null, null, _ID + " ASC", "10");
        cursor.moveToFirst();
        ArrayList<WordEntry> wordEntries = new ArrayList<>();
        WordEntry dictionaryEnId;
        if (cursor.getCount() > 0) {
            do {
                dictionaryEnId = new WordEntry();
                dictionaryEnId.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                dictionaryEnId.setWord(cursor.getString(cursor.getColumnIndexOrThrow(WORD)));
                dictionaryEnId.setTranslation(cursor.getString(cursor.getColumnIndexOrThrow(TRANSLATION)));

                wordEntries.add(dictionaryEnId);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return wordEntries;
    }

    public long insert(WordEntry dictionaryModel, Boolean reverse){
        ContentValues values =  new ContentValues();
        values.put(WORD, dictionaryModel.getWord());
        values.put(TRANSLATION, dictionaryModel.getTranslation());
        return this.database.insert(reverse ? TABLE_NAME_REV : TABLE_NAME, null, values);
    }

    public void beginTransaction(){
        this.database.beginTransaction();
    }

    public void setTransactionSuccess(){
        this.database.setTransactionSuccessful();
    }

    public void endTransaction(){
        this.database.endTransaction();
    }

    public int update(WordEntry dictionaryModel, Boolean reverse) {
        ContentValues args = new ContentValues();
        args.put(WORD, dictionaryModel.getWord());
        args.put(TRANSLATION, dictionaryModel.getTranslation());
        return database.update((reverse ? TABLE_NAME_REV : TABLE_NAME), args,
                _ID + " = '" + dictionaryModel.getId() + "'", null);
    }

    public int delete(int id, Boolean reverse) {
        return database.delete((reverse ? TABLE_NAME_REV : TABLE_NAME),
                _ID + " = '" + id + "'", null);
    }

}
