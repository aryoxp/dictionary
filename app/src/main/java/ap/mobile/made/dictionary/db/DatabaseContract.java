package ap.mobile.made.dictionary.db;

import android.provider.BaseColumns;

class DatabaseContract {

    static String TABLE_NAME = "table_en_id";
    static String TABLE_NAME_REV = "table_id_en";

    static final class DictionaryColumns implements BaseColumns {
        static String _ID = "id";
        static String WORD = "word";
        static String TRANSLATION = "translation";

    }
}
