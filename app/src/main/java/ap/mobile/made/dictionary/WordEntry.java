package ap.mobile.made.dictionary;

import android.os.Parcel;
import android.os.Parcelable;

public class WordEntry implements Parcelable {

    private int id;
    private String word;
    private String translation;

    public WordEntry() {}
    public WordEntry(String word, String translation) {
        this.word = word;
        this.translation = translation;
    }
    private WordEntry(Parcel in) {
        this.id = in.readInt();
        this.word = in.readString();
        this.translation = in.readString();
    }

    public static final Creator<WordEntry> CREATOR = new Creator<WordEntry>() {
        @Override
        public WordEntry createFromParcel(Parcel in) {
            return new WordEntry(in);
        }

        @Override
        public WordEntry[] newArray(int size) {
            return new WordEntry[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(word);
        dest.writeString(translation);
    }
}
